#### Dans chaque collectif qui tente d'oeuvrer sur un projet commun, se pose la question de l'accueil des nouveaux arrivants.

La vie m'a amené en ce moment a me questionner sur ce sujet à travers plusieurs expériences :

1. Une association Fab Lab/Tiers Lieux avec des membres qui adhèrent régulièrement avec un niveau d'engagement variable
1. Une association plus "professionnelle" qui nécessite d'avoir un modèle économique hybride lié à une entreprise ou un groupe d'entrepreneurs, avec des bénévoles et des gens souhaitant en vivre qui rejoignent l'aventure
1. Un atelier participatif avec des citoyens,sur un processus long avec des personnes qui le rejoignent tout au long de la journée
1. Un collectif travaillant sur le pacte pour la transition avec un noyau régulier et la majeure partie des participants aux réunions qui viennent pour la première fois.
1. Le réseau de la transition, rejoint régulièrement par des personnes qui s'activent, poussés par le contexte bizarre dans lequel on se trouve, avec des idées similaires à des choses qui existent déjà ou qui pourraient bénéficier d'expériences existantes.

Dans un premier temps, ce qui me frappe, ce sont les différentes échelles :

**Le temps** : Est-ce un collectif destiné à durer ? Qui existe depuis longtemps ? Qui va durer longtemps ? Si on devait trier les exemples ci-dessus en terme de longévité ce serait 5,1,2,4,3 du moins au plus éphèmère.

**La taille** : Combien de personnes font partie de ce collectif à un instant T ? Est-il destiné à grandir ? Beaucoup ? Trions les exemples ci-dessus par taille du plus grand au plus petit 5,2,1,3,4

Ces 2 paramètres vont jouer sur un troisième paramètre important : 

**le patrimoine informationnel commun du collectif** : Plus le collectif va grandir et plus il va vivre longtemps, plus la quantité d'information, d'expérience, de connaissances générée va être importante.

*Petite aparté : J'aime beaucoup la métaphore organique utilisée par Holochain : chaque information et expérience va s'ajouter au tronc commun du collectif et faire grandir l'arbre, le rendant de plus en plus solide et nourrissant la culture du groupe. Peu à peu, l'information se cristallise, se condense et à travers le temps, crée quelque chose de plus en plus solide.*

Et c'est pour cette raison qu'il faudra porter une attention particulière sur le 4ème paramètre :

**A quel moment dans la vie du collectif la nouvelle personne va rejoindre le groupe ?** Imaginez un train qui accélère peu à peu, si un individu veut monter en route, plus il attendra, plus la vitesse à laquelle il devra être pour rattraper le train devra être élevé. Et on ne peut pas arrêter le train tout le temps, partout, dès que quelqu'un veut monter dedans.

A partir de là, on peut commencer à trier les différents outils disponibles pour l'inclusion des nouvelles personnes :

#### 1ère réunion d'un collectif, 2-50 personnes : Tout le monde est un nouvel arrivant

- Brise glaces : Apprendre à se connaitre, créer un esprit de groupe, créer suffisamment de liens interpersonnels pour que personne ne se sente seul. Créer un climat de sécurité émotionnel propice à la collaboration. 

#### Réunion 2 à X d'un collectif à engagement faible, 2-50 personnes : Le patrimoine informationnel commun a commencé à grandir, le train a pris un peu de vitesse. 

Il est nécessaire ici d'avoir une sorte de sas qui permet aux nouveaux de rattraper leur "retard". L'expression anglaise "Bringing them up to speed" est juste parfaite pour décrire ça.
Pour tous, il est aussi nécessaire à chaque réunion de rappeler le cadre et les pratiques de prise de parole, qui vont mettre encore un petit moment à devenir des habitudes.

- Cercle d'inclusion : pour rappeler le cadre et les pratiques, créer une membrane collective pour la réunion

- Chuchoteurs : Chaque nouvel arrivant sera assigné un parrain, qui pourra lui "décrypter" ce qui se passe, rappeler le contexte, et s'assurer que la personne possède toutes les clés de compréhension pour contribuer.

- Le bocal : Les nouveaux arrivants ne peuvent être qu'observateurs dans un premier temps et ne pourront véritablement participer que lorsqu'ils seront à l'aise.

- Arbre des communs : Un document central (paperboard, fresque, pad, etc...) reprend toutes les informations partagées au sein du collectif, compte rendus, prises de notes, etc... et est disponible au sein de l'espace pour que les nouveaux arrivants puissent se l'approprier avant d'interagir au sein du groupe

- Cercles multiples : Suivant le nombre de participants, il peut être intéressant de créer plusieurs cercles en fonction de l'ancienneté, ou du niveau nécessaire d'implication pour contribuer à la conversation. Par exemple, si un groupe travaille depuis 3 réunions à l'organisation d'un événement, les nouveaux arrivants auront du mal à trouver leur place. Annoncer clairement l'intention de chaque cercle et les conditions requises pour y participer pourront éviter des malentendus et des frustrations.

- Temps de réunion supplémentaire pour les nouveaux arrivants : Pratiqué chez les Colibris, il s'agit ici de demander aux nouveaux de venir 15mn plus tôt pour leur expliquer le contexte, le déroulement de la réunion, la culture du groupe, etc... et leur laisser le temps de s'approprier tout ça, avant que les "vétérans" arrivent.


Lors d'un atelier ponctuel, il convient de prévoir un sas pour les nouveaux arrivants avec une personne dédiée à l'accueil.

Pour les collectifs qui ont un objectif de longévité plus important, les outils numériques vont devenir essentiels dans la structuration du patrimoine informationnel commun. Un wiki, Un blog, tout espace de stockage partagé, seront utiles pour centraliser l'information et la mettre en forme. 
Généralement, l'investissement des membres du collectif augmente également ce qui permet plus de liberté pour gérer leur inclusion et définir un véritable processus d'accueil et d'intégration.

- Formation : Dédier un temps de formation spécial pour expliquer le fonctionnement du collectif, les valeurs, comment trouver les informations, etc...

- Chasse au trésor : Demander à la personne d'organiser des entretiens avec les acteurs clés de l'organisation pour à la fois créer des liens, et comprendre qui sont les référents de différents domaines.

- Cérémonie d'accueil : Présenter les nouvelles personnes au groupe, ou les faire se présenter. Leur offrir de la visibilité auprès du groupe et leur donner l'occasion de partager leurs motivations.

Voilà, c'est tout ce que j'ai pour l'instant. Après avoir fait quelques recherches, j'ai décidé de poster ça ici, vos contributions, bonifications sont les bienvenues. 
Si un wiki commun et ouvert devait apparaitre sur la collaboration, je transfère tout ça avec plaisir pour rendre la contribution plus simple.



- Très bon manuel sur la création d'un cercle d'apprentissage : https://fr.flossmanuals.net/creer-un-cercle-dapprentissage/reussir-linclusion/
- Merci à tous les intervenants sur ce post Facebook, sans qui cet article n'existerait pas, je n'ai fait que condenser ce qu'ils ont dit :-) : https://www.facebook.com/groups/102831306946925/596181974278520/
- Démarrer un projet en gouvernance partagée de Romain Vignes : https://vimeo.com/220835113
