# Pirogues

Définir et regrouper un set d'outils pour déployer facilement et rapidement un cadre de collaboration inter-organisations ou inter-individus qui prend soin du collectif

## Description

Ce projet fait partie des réflexions initiées par les compagnons de l'archipel. Le concept de pirogue est issu de la [pensée archipélique](https://framablog.org/2019/12/10/archipelisation-comment-framasoft-concoit-les-relations-quelle-tisse/).
L'idée est que chaque organisation ou individu est une île et possède une identité en tant que tel. Mais ils possèdent également une identité relation, qui permet d'interagir avec les autres et faire le lien.
Cela permet de ne pas perdre son identité dans la coopération et de s'accorder sur des valeurs communes.
Une pirogue est un projet commun, un outil, un événement, une action qui va être mutualisé par différentes organisations. Un excellent exemple d'un tel type de projet est [Transiscope](https://transiscope.org/), où plusieurs organisations ont mis des ressources en commun pour créer un outil utile à tous.

## Pourquoi ces modèles ?

Dans de nombreux projets, et face aux innombrables "urgences" qu'on ne manque pas de nous rappeler, nous avons tendance à vouloir accélérer, en faire plus, se dépêcher.
Or, généralement, la première chose qui saute quand on se met à courir, c'est l'humain, prendre soin du groupe, attendre les derniers, demander l'avis à tous et se faisant, on se prive de la richesse de l'intelligence collective.

Ces modèles sont là pour que l'urgence ne puisse plus être une excuse pour ne pas prendre soin des gens au sein des projets.
Il s'agit de pouvoir rapidement poser un cadre de collaboration sur lequel s'accorder et qu'on puisse ajuster en fonction des sensibilités. 
A terme, (et s'il y a des motivés), il sera possible également de déployer un cadre juridique plus solide similaire au système [Kalix](http://www.kalix.ch/) développé par Lionel Lourdin en Suisse.

Ce modèle est la première contribution posée comme ressource au sein des archipels. L'idée est bien sûr que d'autres modèles viennent enrichir la bibliothèque de ressources
qui se constitue, au fur et à mesure que de nouveaux projets, de nouvelles formes d'organisations, se concrétisent.

## Modèles existants

[Pirogue Version 0.9](https://gitlab.com/yan-solucracy/pirogues/blob/master/pirogue-V0.9.md) : Ce modèle est imparfait, loin d'être terminé et va faire l'objet de plusieurs réunions pour l'améliorer mais il représente le socle. Vous avez identifié quelque chose qui manque ? N'hésitez pas à créer un ticket ou à bonifier le truc. On peut ajouter par exemple un déroulé type de réunion.

## Pourquoi faire ça sur Gitlab ?

Ces modèles sont tous sous licence CC-By-SA et se veulent évolutifs, et sont de toute façon perfectible. Il doit être possible de créer des branches d'une même pirogue.

Par exemple, la pirogue Version 1.00 peut-être parfaitement adaptée pour un projet entre 2 et 10 organisations/individus, et en partant de ce socle, quelqu'un voudra créer une branche paquebot version 1.00 pour des projets entre 10-50 organisations ou une branche pirogue Evenement, etc....
Gitlab est également un outil parfait pour suivre les contributions de chacun et s'assurer que le crédit soit distribué là où il est dû.

## Comment je peux contribuer ?

Il existe différentes manières de contribuer, et malheureusement la première étape est toujours de [créer un compte sur Gitlab](https://gitlab.com/users/sign_up)

Une fois que c'est fait, vous pouvez 

1.  Dire ce qui manque


* Cliquer sur **Tickets** dans le menu de gauche
* Cliquer sur **Nouveau Ticket** après vous être assuré qu'il n'existait pas déjà
* Remplir le formulaire



2.  Bonifier ce qui existe


Voici comment ça fonctionne :


* Sélectionner un document (par exemple README.md) 
* Cliquer sur Editer
* Cliquer sur Fork (un fork est une branche)

Ici vous allez créer une copie du document chez vous, que vous pourrez modifier, éditer, mettre à jour, etc...

* Quand vos modifications ont été faites, vous pouvez cliquer sur commit, ce qui enregistrera sur votre copie
* Vous pourrez ensuite faire une Pull Request (demande de fusion)

En gros, vous allez envoyer un message aux administrateurs pour leur demander de fusionner vos modifications avec les documents source

* Les admins regardent et approuvent (ou pas en donnant une raison)

Une fois que c'est approuvé, vos modifications apparaitront et tout le monde verra quelles contributions vous avez fait.

## Devenir admin

Si vous voulez devenir admin de ce projet, n'hésitez pas à me contacter à @yan-solucracy , on se fera un petit appel pour en discuter si on ne se connait pas déjà :-)

## Remerciements et inspirations

Ce travail est inspiré de conversations avec les Compagnons de l'archipel, [des chartes des Jardins du Nous](https://grandjardin.jardiniersdunous.org/?ChartE), [du code social](https://codesocial.org) et probablement d'autres trucs que je ne manquerais pas de mentionner ici si ça me reviens :-)


